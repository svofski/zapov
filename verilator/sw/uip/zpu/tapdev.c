/*
 * Copyright (c) 2001, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 * $Id: tapdev.c,v 1.8 2006/06/07 08:39:58 adam Exp $
 */

#define UIP_DRIPADDR0   192
#define UIP_DRIPADDR1   168
#define UIP_DRIPADDR2   0
#define UIP_DRIPADDR3   1

#include "inttypes.h"
#include "uip.h"
#include "xprintf.h"

#define TAP_INIT        1
#define TAP_RX_DONE      2
#define TAP_TX_DONE      4
#define TAP_TX_START     8
#define TAP_RX_READY    16

#define RXOFS_MAX   1800
#define TXOFS_MAX   1800

static volatile uint32_t * const rxbuf = (uint32_t *) 0x8000;
static volatile uint32_t * const txbuf = (uint32_t *) 0x9000;
static volatile uint32_t * const csr = (uint32_t *)    0x02000000;
static volatile uint32_t * const rxsize = (uint32_t *) 0x02000004;
static volatile uint32_t * const txsize = (uint32_t *) 0x02000008;

static uint32_t tx_ofs;
static uint32_t * uip_buf32 = (uint32_t *) uip_buf;

static struct _rx {
    enum { RX_NEW, RX_READING } state;
    int offset;
    int avail;
} rx;

/*---------------------------------------------------------------------------*/
void
tapdev_init(void)
{
    *csr = 0;
    *csr ^= TAP_INIT;
    *csr ^= TAP_INIT;
    *csr = TAP_RX_READY;

    tx_ofs = 0;

    rx.state = RX_NEW;
    rx.offset = 0;
    rx.avail = 0;

    xprintf("tapdev_init: UIP_BUFSIZE=%d\n", UIP_BUFSIZE);
}

void phex8(uint8_t x)
{
    extern int putchar(int);
    int n = 0x0f & (x >> 4);
    if (n < 10) putchar('0' + n); else putchar(('a' - 10) + n);
    n = 0x0f & x;
    if (n < 10) putchar('0' + n); else putchar(('a' - 10) + n);
}

void phex32(uint32_t x)
{
    phex8(x>>24);
    phex8(x>>16);
    phex8(x>>8);
    phex8(x);
}

/*---------------------------------------------------------------------------*/
unsigned int
tapdev_read(void)
{
    int cnt = 0;

    if (rx.state == RX_NEW) {
        if (*csr & TAP_RX_DONE) {
            rx.state = RX_READING;
            rx.avail = *rxsize;
            rx.offset = 0;
        }
    }

    // fallthrough because the state changes
    if (rx.state == RX_READING) {
        cnt = rx.avail;
        rx.avail = (rx.avail  + 3) & ~3;

        for (int i = 0; i < UIP_BUFSIZE/4 && rx.avail > 0; ++i) {
            uip_buf32[i] = rxbuf[rx.offset];
            //xprintf(" Z:"); phex32(uip_buf32[i]);
            //xprintf("Z:%08d ", uip_buf32[i]);
            rx.offset += 1;
            rx.avail -= 4;
        }

        if (rx.avail <= 0) {
            xprintf("Z: received %d octets\n", cnt);
            rx.state = RX_NEW;
            rx.avail = 0;
            *csr |= TAP_RX_READY;
            *csr &= ~TAP_RX_READY;
        }
    }

    return cnt;
}
/*---------------------------------------------------------------------------*/
void
tapdev_send(void)
{
    xprintf("Z: tapdev_send %d bytes\n", uip_len);
    //while ((*csr & TAP_TX_DONE) == 0);
    memcpy(txbuf, uip_buf, uip_len);
    *txsize = uip_len;
    *txsize = uip_len;
    *txsize = uip_len;
    *txsize = uip_len;
    *csr |= TAP_TX_START;
    *csr ^= TAP_TX_START;
}
/*---------------------------------------------------------------------------*/
