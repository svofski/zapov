//
// Run the processor with memory attached.
//
// Copyright (c) 2018 Serge Vakulenko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
`timescale 1 ns / 1 ps
`default_nettype none

parameter ENABLE_TRACE = 0;

module zpu4(
    input wire clk,
    input wire resetq,
    output wire uart0_wr,
    output wire uart0_rd,
    output wire [7:0] uart0_channel,
    input wire uart0_valid,
    input wire [7:0] uart0_data,
    
    output reg [15:0]   eth_txlen,
    output reg          eth_txstart,
    output reg          eth_init,
    input wire          eth_txdone,
    input wire [15:0]   eth_rxlen,
    input wire          eth_rxdone,
    output wire         eth_rxready,

    output wire [31:0] gpio,
    output wire test_cpu_memrd,
    output wire test_sel_ram
);

assign test_cpu_memrd = cpu_memrd;
assign test_sel_ram = sel_ram;

wire        i_interrupt;   // interrupt request
wire        cpu_mem_done = ram_done;  // memory operation completed
wire [31:0] cpu_di;        // data to cpu

// Outputs.
wire        cpu_memrd;         // read op
wire        cpu_memwr;         // write op
wire [31:0] cpu_addr;          // address output
wire [31:0] cpu_do;            // data from cpu

wire [3:0] mem_write_mask;
wire cpu_break;

zpu_core zpu4(
    .clk(clk),
    .reset(resetq),
    .enable(1),
    .in_mem_busy(mem_busy),
    .mem_read(cpu_di),
    .mem_write(cpu_do),
    .out_mem_addr(cpu_addr),
    .out_mem_writeEnable(cpu_memwr),
    .out_mem_readEnable(cpu_memrd),
    .mem_writeMask(mem_write_mask),
    .interrupt(i_interrupt),
    .o_break(cpu_break)
);

wire mem_busy = cpu_memrd | cpu_memwr | ram_done | io_done;

wire sel_io = cpu_addr[31:20] != 0;
wire sel_ram = cpu_addr[31:20] == 0;

wire [19:0] ram_addr = cpu_addr[19:0];
wire [31:0] ram_di = cpu_do;
wire [31:0] ram_do;
wire        ram_wr = cpu_memwr & sel_ram;
wire        ram_rd = cpu_memrd & sel_ram;
wire        ram_done;


assign cpu_di = sel_ram ? ram_do : io_do;

//always @(cpu_addr) begin
//    if (zpu4.pc == 'hc9f) begin
//        $display("io reading: cpu_addr=%08x cpu_di=%08x", cpu_addr, cpu_di);
//    end
//end
//
// 1Mword x 32bit of RAM.
memory ram(
    .clk(clk),                      // clock on rising edge
    //.i_addr(ram_addr[19:0]),        // input address
    .i_addr({2'b00,ram_addr[19:2]}),        // input address
    .i_read(ram_rd),                // input read request
    .i_write(ram_wr),               // input write request
    .i_data(ram_di),                // input data to memory
    .o_data(ram_do),                // output data from memory
    .o_done(ram_done)               // output r/w operation completed
);

// 31     23 222120
// xxxx xxxx x x x
wire [7:0] io_device = cpu_addr[31:24];

reg io_done;
reg [31:0] io_do = 0;
wire io_wr = cpu_memwr & sel_io;
wire io_rd = cpu_memrd & sel_io;

always @(posedge clk) begin
    if (io_rd) begin
        if (eth_csr_sel)
            io_do <= eth_csr_do;
        else if (eth_rxsize_sel)
            io_do <= {16'b0, eth_rxlen};
        else if (eth_txsize_sel)
            io_do <= {16'b0, eth_txlen};
        else if (uart0_rd)
            io_do <= uart0_do;
    end
    if (io_wr) begin
        if (eth_csr_wr) begin
            eth_init <= cpu_do[0];      // 1
            eth_txstart <= cpu_do[3];   // 8
            eth_rxready <= cpu_do[4];   // 16
            //$display("eth_rxready=", cpu_do[4]);
            //$display("pc=%08x: eth_csr_wr: cpu_do=%08x init=%d txstart=%d, eth_csr_do=%08x", 
            //    zpu4.pc,
            //    cpu_do, cpu_do[0], cpu_do[3], eth_csr_do);
        end
        if (io_wr && eth_txsize_sel) begin
            eth_txlen <= cpu_do[15:0];
        end
    end
    io_done <= io_rd | io_wr;
end

// rxready: 1 -> rxdone = 0
// rxdone: 1 -> rxready = 0

//wire [31:0] io_do = 
//    uart0_rd ? uart0_do :
//    eth_csr_rd ? eth_csr_do : 'hbebebebe;

// ######   UART   ##########################################

assign uart0_wr = io_wr && io_device == 1;
assign uart0_rd = io_rd && io_device == 1;
assign uart0_channel = cpu_do[31:24];//cpu_do[7:0];

//always @(posedge clk) if (uart0_wr) begin
//    $display("UART0:", cpu_do);
//end

wire [31:0] uart0_do = io_device == 1 ? {24'b0, uart0_data} :
    io_device == 2 ? {24'b0, 6'b0, uart0_valid, 1'b1} : 32'b0;

// ####### ETHERNET #####   #### #  ##### ###

wire eth_csr_sel = io_device == 2 && cpu_addr[15:0] == 0;
wire eth_rxsize_sel = io_device == 2 && cpu_addr[15:0] == 4;
wire eth_txsize_sel = io_device == 2 && cpu_addr[15:0] == 8;

wire eth_csr_wr = io_wr && io_device == 2;
wire eth_csr_rd = io_rd && io_device == 2 && cpu_addr[15:0] == 0;

wire [31:0] eth_csr_do = 
    {24'b0, 4'b0000, eth_txstart, eth_txdone, eth_rxdone, eth_init};

assign gpio = {29'b0, cpu_memwr, cpu_memrd, cpu_memrd};

// ---- opcode disassembler ---
always @(posedge clk) begin
    if (ENABLE_TRACE && zpu4.state == 6) begin
        $write("pc=%04x sp=%08x ", zpu4.pc, zpu4.sp<<2);
        case (zpu4.insn)
            Insn_AddTop           : $display("---Insn_AddTop          ");
            Insn_Dup              : $display("---Insn_Dup             ");
            Insn_DupStackB        : $display("---Insn_DupStackB       ");
            Insn_Pop              : $display("---Insn_Pop             ");
            Insn_PopDown          : $display("---Insn_PopDown         ");
            Insn_Add              : $display("---Insn_Add             ");
            Insn_Or               : $display("---Insn_Or              ");
            Insn_And              : $display("---Insn_And             ");
            Insn_Store            : $display("---Insn_Store           ");
            Insn_AddSP            : $display("---Insn_AddSP           ");
            Insn_Shift            : $display("---Insn_Shift           ");
            Insn_Nop              : $display("---Insn_Nop             ");
            Insn_Im               : $display("---Insn_Im 0x%h",
                zpu4.opcode[zpu4.pc[1:0]][6:0]);
            Insn_LoadSP           : $display("---Insn_LoadSP          ");
            Insn_StoreSP          : $display("---Insn_StoreSP         ");
            Insn_Emulate          : $display("---Insn_Emulate         ");
            Insn_Load             : $display("---Insn_Load            ");
            Insn_PushSP           : $display("---Insn_PushSP          ");
            Insn_PopPC            : $display("---Insn_PopPC           ");
            Insn_PopPCrel         : $display("---Insn_PopPCrel        ");
            Insn_Not              : $display("---Insn_Not             ");
            Insn_Flip             : $display("---Insn_Flip            ");
            Insn_PopSP            : $display("---Insn_PopSP           ");
            Insn_Neqbranch        : $display("---Insn_Neqbranch       ");
            Insn_Eq               : $display("---Insn_Eq              ");
            Insn_Loadb            : $display("---Insn_Loadb           ");
            Insn_Mult             : $display("---Insn_Mult            ");
            Insn_Lessthan         : $display("---Insn_Lessthan        ");
            Insn_Lessthanorequal  : $display("---Insn_Lessthanorequal ");
            Insn_Ulessthanorequal : $display("---Insn_Ulessthanorequal");
            Insn_Ulessthan        : $display("---Insn_Ulessthan       ");
            Insn_PushSPadd        : $display("---Insn_PushSPadd       ");
            Insn_Call             : $display("---Insn_Call            ");
            Insn_CallPCrel        : $display("---Insn_CallPCrel       ");
            Insn_Sub              : $display("---Insn_Sub             ");
            Insn_Break            : $display("---Insn_Break           ");
            Insn_Storeb           : $display("---Insn_Storeb          ");
            Insn_InsnFetch        : $display("---Insn_InsnFetch       ");
        endcase
    end
end

endmodule
