#include <stdio.h>
#include <vector>
#include "Vzpu4.h"
#include "verilated_vcd_c.h"

// ------ tap ------
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <sys/socket.h>

#ifdef linux
#include <sys/ioctl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#define DEVTAP "/dev/net/tun"
#else  /* linux */
#define DEVTAP "/dev/tap0"
#endif /* linux */

uint32_t bigend(uint32_t le)
{
    return ((le & 0xff000000) >> 24) | ((le & 0x00ff0000) >> 8) |
        ((le & 0x0000ff00) << 8) | ((le & 0x000000ff) << 24);
}

void dump(std::vector<uint8_t> buf, int ret)
{
    printf("HOST: read %d bytes\n", ret);
    for (int i = 0; i < ret + 16; i += 16) {
        for (int j = 0; j < 16; ++j) {
            if (i + j < ret) {
                printf("%02x%c", buf[i+j], j == 7 ? '-' : ' ');
            }
            else {
                printf("   ");
            }
        }
        printf("  ");
        for (int j = 0; j < 16; ++j) {
            if (i + j < ret) {
                int c = buf[i+j];
                printf("%c", (c >= 0x20 && c < 0x7f) ? c : '.');
            }
            else {
                printf(" ");
            }
        }
        printf("\n");
    }
}

struct TapDriver
{
    int fd;
    std::vector<uint8_t> buf;

    TapDriver() : fd(-1), buf(512) {}


    void init()
    {
        if (fd != -1) return;

        fd = open(DEVTAP, O_RDWR);
        printf("DEVTAP is: %s, fd=%d\n", DEVTAP, fd);
        if(fd == -1) {
            perror("tapdev: tapdev_init: open");
            exit(1);
        }

        {
            struct ifreq ifr;
            memset(&ifr, 0, sizeof(ifr));
            strncpy(ifr.ifr_name, "tap0", sizeof(ifr.ifr_name));
            ifr.ifr_name[sizeof(ifr.ifr_name)-1] = 0; /* ensure \0 termination */

            ifr.ifr_flags = IFF_TAP|IFF_NO_PI;
            if (ioctl(fd, TUNSETIFF, (void *) &ifr) < 0) {
                perror("tapif_init: " DEVTAP " ioctl TUNSETIFF");
                exit(1);
            }
        }
    }

    unsigned poll()
    {
        if (fd == -1) return 0;

        fd_set fdset;
        struct timeval tv, now;
        int ret;

        tv.tv_sec = 0;
        tv.tv_usec = 1000;


        FD_ZERO(&fdset);
        FD_SET(fd, &fdset);

        ret = select(fd + 1, &fdset, NULL, NULL, &tv);
        if(ret == 0) {
            return 0;
        }
        memset(&buf[0], 255, buf.size());
        ret = ::read(fd, &buf[0], buf.size());

        if(ret == -1) {
            perror("tap_dev: tapdev_read: read");
        }

        dump(buf, ret);

        // swap byte order
        uint32_t * buf32 = (uint32_t *) &buf[0];
        for (int i = 0; i < ret; i += 4) {
            buf32[i/4] = bigend(buf32[i/4]);
        }

        return ret;
    }

    void send(std::vector<uint8_t>& buf)
    {
        int ret;
        printf("tapdev_send: sending %ld bytes\n", buf.size());
        /*  check_checksum(uip_buf, size);*/

        uint32_t * buf32 = (uint32_t *)&buf[0];
        for (int i = 0; i < buf.size()/4; ++i) {
            buf32[i] = bigend(buf32[i]);
        }

        /*  drop++;
            if(drop % 8 == 7) {
            printf("Dropped a packet!\n");
            return;
            }*/
        ret = write(fd, &buf[0], buf.size());
        if(ret == -1) {
            perror("tap_dev: tapdev_send: writev");
            exit(1);
        }
    }

};
// ------ tap ----

int main(int argc, char** argv)
{
    Verilated::commandArgs(argc, argv);
    Vzpu4* top = new Vzpu4;

    TapDriver tap;

    if (argc == 2) {
        fprintf(stderr, "loading ram from %s\n", argv[1]);
        FILE* hex = fopen(argv[1], "r");

        if (!hex) {
            perror("could not read file");
            exit(1);
        }

        fseek(hex, 0, SEEK_END);
        size_t size = ftell(hex);
        rewind(hex);

        std::vector<uint32_t> binary(size / sizeof(uint32_t));
        int count = fread(&binary[0], sizeof(uint32_t), binary.size(), hex);
        printf("read %d records\n", count);
        for (int i = 0; i < binary.size(); i++) {
            uint32_t le = binary[i];
            uint32_t be = ((le & 0xff000000) >> 24) | ((le & 0x00ff0000) >> 8) |
                          ((le & 0x0000ff00) << 8) | ((le & 0x000000ff) << 24);
            top->zpu4__DOT__ram__DOT__mem[i] = be;
            // printf("%04x: little: %08x big: %08x\n", i*4, le, be);
        }
    }

    top->resetq = 1;
    top->zpu4__DOT__i_interrupt = 0;
    for (int i = 0; i < 10; ++i) {
        top->clk = 1;
        top->eval();
        top->clk = 0;
        top->eval();
    }
    top->resetq = 0;
    top->uart0_valid = 1; // pretend to always have a character waiting

    std::pair<IData,IData> sp_limits {0x8000, 0x8000};
    int cycle;
    int eth_rxready_prev = 0, eth_txstart_prev = 0;
    for (cycle = 0;; cycle++) {
        top->clk = 1;
        top->eval();
        top->clk = 0;
        top->eval();

        std::pair<IData,IData> newlimits{
            std::min(sp_limits.first, top->zpu4__DOT__zpu4__DOT__sp),
            std::max(sp_limits.second, top->zpu4__DOT__zpu4__DOT__sp)};


        if (top->uart0_wr) {
            //printf("\nuart0: %d %x %c\n", top->uart0_channel,
            //  top->uart0_channel, top->uart0_channel);
            putchar(top->uart0_channel);
        }

        if (top->eth_init) {
            tap.init();
        }

        if (eth_rxready_prev == 0 && top->eth_rxready) {
            top->eth_rxdone = 0;
            printf("H: resetting eth_rxdone\n");
        }
        eth_rxready_prev = top->eth_rxready;

        if (cycle % 8192 == 0) {
            int recvcnt = tap.poll();
            if (recvcnt) {
                int rounded = (recvcnt + 3) & ~3;
                if (!top->eth_rxdone) {
                    memcpy(&top->zpu4__DOT__ram__DOT__mem[0x8000/4],
                            &tap.buf[0], rounded);
                    top->eth_rxdone = 1;
                    top->eth_rxlen = recvcnt;
                    //uint32_t * mem32 = (uint32_t*) &top->zpu4__DOT__ram__DOT__mem[0x8000/4];
                    //for (int i = 0; i < 8; ++i) {
                    //    printf("M:%08x ", mem32[i]);
                    //}
                }
                else {
                    printf("H: could not marshall because top->eth_rxdone=1\n");
                }
            }
        }

        if (eth_txstart_prev == 0 && top->eth_txstart) {
            printf("H: eth_txstart\n");
            int len = (top->eth_txlen + 3) & ~3;
            std::vector<uint8_t> sendbuf(len);
            memcpy(&sendbuf[0], &top->zpu4__DOT__ram__DOT__mem[0x9000/4], len);
            tap.send(sendbuf);
        }
        eth_txstart_prev = top->eth_txstart;
    }
    printf("Simulation ended after %d cycles\n", cycle);
    delete top;

    return 0;
}
