// ZPU
//
// Copyright 2004-2008 oharboe - �yvind Harboe - oyvind.harboe@zylin.com
// Copyright 2008 alvieboy - �lvaro Lopes - alvieboy@alvie.com
// 
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the ZPU Project.
// mem_writeEnable - set to '1' for a single cycle to send off a write request.
//                   mem_write is valid only while mem_writeEnable='1'.
// mem_readEnable - set to '1' for a single cycle to send off a read request.
// 
// mem_busy - It is illegal to send off a read/write request when mem_busy='1'.
//            Set to '0' when mem_read  is valid after a read request.
//            If it goes to '1'(busy), it is on the cycle after mem_read/writeEnable
//            is '1'.
// mem_addr - address for read/write request
// mem_read - read data. Valid only on the cycle after mem_busy='0' after 
//            mem_readEnable='1' for a single cycle.
// mem_write - data to write
// mem_writeMask - set to '1' for those bits that are to be written to memory upon
//                 write request
// break - set to '1' when CPU hits break instruction
// interrupt - set to '1' until interrupts are cleared by CPU. 
// no timescale needed
//
//
// SystemVerilog Translation by Viacheslav Slavinsky svofski@gmail.com, 2020
//
// This core was translated by hand with help of vhd2vl.
// There is a lot of hand work and fixing up involved.
//
// It is good enough to run uIP. 
// Expect surprises.
//

typedef enum
{
    Insn_AddTop,
    Insn_Dup,
    Insn_DupStackB,
    Insn_Pop,
    Insn_PopDown,
    Insn_Add,
    Insn_Or,
    Insn_And,
    Insn_Store,
    Insn_AddSP,
    Insn_Shift,
    Insn_Nop,
    Insn_Im,
    Insn_LoadSP,
    Insn_StoreSP,
    Insn_Emulate,
    Insn_Load,
    Insn_PushSP,
    Insn_PopPC,
    Insn_PopPCrel,
    Insn_Not,
    Insn_Flip,
    Insn_PopSP,
    Insn_Neqbranch,
    Insn_Eq,
    Insn_Loadb,
    Insn_Mult,
    Insn_Lessthan,
    Insn_Lessthanorequal,
    Insn_Ulessthanorequal,
    Insn_Ulessthan,
    Insn_PushSPadd,
    Insn_Call,
    Insn_CallPCrel,
    Insn_Sub,
    Insn_Break,
    Insn_Storeb,
    Insn_InsnFetch
} insn_type;

parameter OpCode_Im              =  'b1;
parameter OpCode_StoreSP         =  'b010;
parameter OpCode_LoadSP          =  'b011;
parameter OpCode_Emulate         =  'b001;
parameter OpCode_AddSP           =  'b0001;
parameter OpCode_Short           =  'b0000;

parameter OpCode_Break           =  'b0000;
parameter OpCode_NA4             =  'b0001;
parameter OpCode_PushSP          =  'b0010;
parameter OpCode_NA3             =  'b0011;

parameter OpCode_PopPC           =  'b0100;
parameter OpCode_Add             =  'b0101;
parameter OpCode_And             =  'b0110;
parameter OpCode_Or              =  'b0111;

parameter OpCode_Load            =  'b1000;
parameter OpCode_Not             =  'b1001;
parameter OpCode_Flip            =  'b1010;
parameter OpCode_Nop             =  'b1011;

parameter OpCode_Store           =  'b1100;
parameter OpCode_PopSP           =  'b1101;
parameter OpCode_NA2             =  'b1110;
parameter OpCode_NA              =  'b1111;

parameter OpCode_Lessthan        =  36;
parameter OpCode_Lessthanorequal =  37;
parameter OpCode_Ulessthan       =  38;
parameter OpCode_Ulessthanorequal=  39;

parameter OpCode_Swap            =  40;
parameter OpCode_Mult            =  41;

parameter OpCode_Lshiftright     =  42;
parameter OpCode_Ashiftleft      =  43; // 0x2b
parameter OpCode_Ashiftright     =  44;
parameter OpCode_Call            =  45;

parameter OpCode_Eq              =  46;
parameter OpCode_Neq             =  47;

parameter OpCode_Sub             =  49;
parameter OpCode_Loadb           =  51;
parameter OpCode_Storeb          =  52;

parameter OpCode_Eqbranch        =  55;
parameter OpCode_Neqbranch       =  56;
parameter OpCode_PopPCRel        =  57;

parameter OpCode_Pushspadd       =  61;
parameter OpCode_Mult16x16       =  62;
parameter OpCode_Callpcrel       =  63;


module zpu_core
#(
    parameter integer wordSize = 32,
    parameter integer maxAddrBitIncIO = 31,
    parameter integer wordBytes = 4,
    parameter integer byteBits = 2,
    parameter integer minAddrBit = 2,
    parameter integer OpCode_Size = 8,
    parameter reg [maxAddrBitIncIO:0] spStart = 'h8000
)
(
input wire clk,
input wire reset,
input wire enable,
input wire in_mem_busy,
input wire [wordSize - 1:0] mem_read,
output reg [wordSize - 1:0] mem_write,
output wire [maxAddrBitIncIO:0] out_mem_addr,
output wire out_mem_writeEnable,
output wire out_mem_readEnable,
output reg [wordBytes - 1:0] mem_writeMask,
input wire interrupt,
output reg o_break
);

typedef enum
{
    State_Load2 = 0,
    State_Popped = 1,
    State_LoadSP2 = 2,
    State_LoadSP3 = 3,
    State_AddSP2 = 4,
    State_Fetch = 5,
    State_Execute = 6,
    State_Decode = 7,
    State_Decode2 = 8,
    State_Resync = 9,

    State_StoreSP2 = 10,
    State_Resync2 = 11,
    State_Resync3 = 12,
    State_Loadb2 = 13,
    State_Storeb2 = 14,
    State_Mult2 = 15,
    State_Mult3 = 16,
    State_Mult5 = 17,
    State_Mult4 = 18,
    State_BinaryOpResult2 = 19,
    State_BinaryOpResult = 20,
    State_Idle = 21,
    State_Interrupt = 22
} state_type;

reg idim_flag;
reg mem_writeEnable;
reg mem_readEnable;
reg [maxAddrBitIncIO:minAddrBit] mem_addr;
reg [maxAddrBitIncIO:minAddrBit] mem_delayAddr;
reg mem_delayReadEnable;
reg inInterrupt;
reg [wordSize-1:0] decodeWord;

reg [maxAddrBitIncIO:0] pc;
reg [maxAddrBitIncIO:minAddrBit] sp;
reg [maxAddrBitIncIO:minAddrBit] incSp;
reg [maxAddrBitIncIO:minAddrBit] incIncSp;
reg [maxAddrBitIncIO:minAddrBit] decSp;
reg [maxAddrBitIncIO:0] stackA;
reg [maxAddrBitIncIO:0] binaryOpResult;
reg [maxAddrBitIncIO:0] binaryOpResult2;
reg [maxAddrBitIncIO:0] multResult;
reg [maxAddrBitIncIO:0] multResult2;
reg [maxAddrBitIncIO:0] multResult3;
reg [maxAddrBitIncIO:0] multA;
reg [maxAddrBitIncIO:0] multB;
reg [maxAddrBitIncIO:0] stackB;

state_type state;

reg [7:0] mem_byte[0:3];
for (genvar i = 0; i < 4; i = i + 1) begin
    always_comb
    begin
        mem_byte[i] = mem_read[(4-1-i) * 8 + 7:(4-1-i) * 8];
    end
end

wire [7:0] opcode[0:wordBytes - 1];  
insn_type decodedOpcode[0:wordBytes-1];
insn_type tDecodedOpcode[0:wordBytes-1];
insn_type insn;

wire [31:0] byteofs = wordBytes - 1 - {30'b0,stackA[byteBits-1:0]};

reg begin_inst;

decoder #(.wordSize(wordSize), .wordBytes(wordBytes)) 
    d0(.decodeWord(decodeWord), .opcode(opcode),
    .tDecodedOpcode(tDecodedOpcode));

  // the memory subsystem will tell us one cycle later whether or 
  // not it is busy
  assign out_mem_writeEnable = mem_writeEnable;
  assign out_mem_readEnable = mem_readEnable;
  assign out_mem_addr[maxAddrBitIncIO:minAddrBit] = mem_addr;
  assign out_mem_addr[minAddrBit - 1:0] = {(((minAddrBit - 1))-((0))+1){1'b0}};
  assign incSp = sp + 1;
  assign incIncSp = sp + 2;
  assign decSp = sp - 1;

  always @(posedge clk or posedge reset) begin : P1
    reg [maxAddrBitIncIO:0] nextPC;
    reg [wordSize * 2 - 1:0] tMultResult;
    reg [4:0] spOffset;

    if(reset == 1'b 1) begin
      state <= State_Idle;
      o_break <= 1'b 0;
      sp <= spStart[maxAddrBitIncIO:minAddrBit];
      pc <= {(((maxAddrBitIncIO))-((0))+1){1'b0}};
      idim_flag <= 1'b 0;
      begin_inst <= 1'b 0;
      inInterrupt <= 1'b 0;
      mem_writeEnable <= 1'b 0;
      mem_readEnable <= 1'b 0;
      multA <= {(((wordSize - 1))-((0))+1){1'b0}};
      multB <= {(((wordSize - 1))-((0))+1){1'b0}};
      mem_writeMask <= {(((wordBytes - 1))-((0))+1){1'b1}};
    end else begin
      //$display("state: ", state.name(), " pc=", pc, " sp=", sp << 2, 
      //  " insn=", insn.name(), " mem_busy=", in_mem_busy);
      // we must multiply unconditionally to get pipelined multiplication
      tMultResult = multA * multB;
      multResult3 <= multResult2;
      multResult2 <= multResult;
      multResult <= tMultResult[wordSize - 1:0];
      binaryOpResult2 <= binaryOpResult;
      // pipeline a bit.
      multA <= {(((wordSize - 1))-((0))+1){1'bX}};
      multB <= {(((wordSize - 1))-((0))+1){1'bX}};
      mem_addr <= {(((maxAddrBitIncIO))-((minAddrBit))+1){1'bX}};
      mem_readEnable <= 1'b 0;
      mem_writeEnable <= 1'b 0;
      mem_write <= {(((wordSize - 1))-((0))+1){1'bX}};
      if((mem_writeEnable == 1'b 1) && (mem_readEnable == 1'b 1)) begin
            $display("read/write collision");
      end
      spOffset[4] =  ~opcode[pc[byteBits - 1:0]][4];
      spOffset[3:0] = opcode[pc[byteBits - 1:0]][3:0];
      nextPC = pc + 1;
      begin_inst <= 1'b 0;
      if((interrupt == 1'b 0)) begin
        // Interrupt ended, we can serve ISR again
        inInterrupt <= 1'b 0;
      end
      case(state)
      State_Idle : begin
        if(enable == 1'b 1) begin
          state <= State_Resync;
        end
        // Initial state of ZPU, fetch top of stack + first instruction 
      end
      State_Resync : begin
        //$display("Resync: in_mem_busy=", in_mem_busy);
        if(in_mem_busy == 1'b 0) begin
          mem_addr <= sp;
          mem_readEnable <= 1'b 1;
          state <= State_Resync2;
        end
      end
      State_Resync2 : begin
        if(in_mem_busy == 1'b 0) begin
          stackA <= mem_read;
          mem_addr <= incSp;
          mem_readEnable <= 1'b 1;
          state <= State_Resync3;
        end
      end
      State_Resync3 : begin
        if(in_mem_busy == 1'b 0) begin
          stackB <= mem_read;
          mem_addr <= pc[maxAddrBitIncIO:minAddrBit];
          mem_readEnable <= 1'b 1;
          state <= State_Decode;
        end
      end
      State_Decode : begin
        //  $display("State_Decode: mem_busy=", in_mem_busy, " mem_read=", 
        //      mem_read);
        if (in_mem_busy == 1'b 0) begin
          decodeWord <= mem_read;
          state <= State_Decode2;
          // Do not recurse into ISR while interrupt line is active
          if(interrupt == 1'b 1 && inInterrupt == 1'b 0 && idim_flag == 1'b 0) begin
            // We got an interrupt, execute interrupt instead of next instruction
            inInterrupt <= 1'b 1;
            sp <= decSp;
            mem_writeEnable <= 1'b 1;
            mem_addr <= incSp;
            mem_write <= stackB;
            stackA <= {(((wordSize - 1))-((0))+1){1'bX}};
            stackA[maxAddrBitIncIO:0] <= pc;
            stackB <= stackA;
            pc <= maxAddrBitIncIO + 1;
            state <= State_Interrupt;
          end
          // interrupt
        end
        // in_mem_busy
      end
      State_Interrupt : begin
        if(in_mem_busy == 1'b 0) begin
          mem_addr <= (pc[maxAddrBitIncIO:minAddrBit]);
          mem_readEnable <= 1'b 1;
          state <= State_Decode;
          //svo report "ZPU jumped to interrupt!" severity note;
        end
      end
      State_Decode2 : begin
        // tDecodedOpcode is an output of module decoder()
        insn <= tDecodedOpcode[pc[byteBits - 1:0]];
        // once we wrap, we need to fetch
        decodedOpcode <= tDecodedOpcode;
        decodedOpcode[0] <= Insn_InsnFetch;
        state <= State_Execute;
        // Each instruction must:
        //
        // 1. set idim_flag
        // 2. increase PC if applicable
        // 3. set next state if appliable
        // 4. do it's operation
      end
      State_Execute : begin
        // the original always overwrites insn, but it causes problems
        // example: nop \ im 1 \ nop \ im 2
        // my simple solution is to only update insn when the pc is updated

        //$display("State_Execute: insn <= ", insn.name(), " pc=", pc,
        //   " next insn =", decodedOpcode[nextPC[byteBits - 1:0]].name(), 
        //   " nextPC=", nextPC);
        case(insn)
        Insn_InsnFetch : begin
          state <= State_Fetch;
        end
        Insn_Im : begin
          if (in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 1;
            pc <= pc + 1;
            if (idim_flag == 1'b 1) begin
              mem_writeEnable <= 1'b 1; // the original had no this line
              stackA[wordSize - 1:7] <= stackA[wordSize - 8:0];
              stackA[6:0] <= opcode[pc[byteBits - 1:0]][6:0];
            end
            else begin
              mem_writeEnable <= 1'b 1;
              mem_addr <= incSp;
              mem_write <= stackB;
              stackB <= stackA;
              sp <= decSp;
              for (integer i=wordSize - 1; i >= 7; i = i - 1) begin
                stackA[i] <= opcode[pc[byteBits - 1:0]][6];
              end
              stackA[6:0] <= opcode[pc[byteBits - 1:0]][6:0];
            end
            // idim_flag
          end
          //else $display("NOT EXECUTED Insn_Im ", opcode[pc[byteBits - 1:0]][6:0]);
          // in_mem_busy
        end
        Insn_StoreSP : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            state <= State_StoreSP2;
            mem_writeEnable <= 1'b 1;
            mem_addr <= sp + {25'b0,spOffset};
            mem_write <= stackA;
            stackA <= stackB;
            sp <= incSp;
          end
        end
        Insn_LoadSP : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            state <= State_LoadSP2;
            sp <= decSp;
            mem_writeEnable <= 1'b 1;
            mem_addr <= incSp;
            mem_write <= stackB;
          end
        end
        Insn_Emulate : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            sp <= decSp;
            mem_writeEnable <= 1'b 1;
            mem_addr <= (incSp);
            mem_write <= (stackB);
            stackA <= {(((wordSize - 1))-((0))+1){1'bX}};
            stackA[maxAddrBitIncIO:0] <= pc + 1;
            stackB <= stackA;
            // The emulate address is:
            //        98 7654 3210
            // 0000 00aa aaa0 0000
            pc <= {(((maxAddrBitIncIO))-((0))+1){1'b0}};
            pc[9:5] <= opcode[pc[byteBits - 1:0]][4:0];
            state <= State_Fetch;
          end
          // in_mem_busy
        end
        Insn_CallPCrel : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            stackA <= {(((wordSize - 1))-((0))+1){1'bX}};
            stackA[maxAddrBitIncIO:0] <= pc + 1;
            pc <= pc + stackA[maxAddrBitIncIO:0];
            state <= State_Fetch;
          end
        end
        Insn_Call : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            stackA <= {(((wordSize - 1))-((0))+1){1'bX}};
            stackA[maxAddrBitIncIO:0] <= pc + 1;
            pc <= stackA[maxAddrBitIncIO:0];
            state <= State_Fetch;
          end
        end
        Insn_AddSP : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            state <= State_AddSP2;
            mem_readEnable <= 1'b 1;
            mem_addr <= sp + {25'b0,spOffset};
          end
        end
        Insn_PushSP : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            pc <= pc + 1;
            sp <= decSp;
            stackA <= {(((wordSize - 1))-((0))+1){1'b0}};
            stackA[maxAddrBitIncIO:minAddrBit] <= sp;
            stackB <= stackA;
            mem_writeEnable <= 1'b 1;
            mem_addr <= (incSp);
            mem_write <= (stackB);
          end
        end
        Insn_PopPC : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            pc <= stackA[maxAddrBitIncIO:0];
            sp <= incSp;
            mem_writeEnable <= 1'b 1;
            mem_addr <= (incSp);
            mem_write <= (stackB);
            state <= State_Resync;
          end
        end
        Insn_PopPCrel : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            pc <= stackA[maxAddrBitIncIO:0] + pc;
            sp <= incSp;
            mem_writeEnable <= 1'b 1;
            mem_addr <= (incSp);
            mem_write <= (stackB);
            state <= State_Resync;
          end
        end
        Insn_Add : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            stackA <= stackA + stackB;
            mem_readEnable <= 1'b 1;
            mem_addr <= (incIncSp);
            sp <= incSp;
            state <= State_Popped;
          end
        end
        Insn_Sub : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            binaryOpResult <= stackB - stackA;
            state <= State_BinaryOpResult;
          end
        end
        Insn_Pop : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            mem_addr <= (incIncSp);
            mem_readEnable <= 1'b 1;
            sp <= incSp;
            stackA <= stackB;
            state <= State_Popped;
          end
        end
        Insn_PopDown : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            // PopDown leaves top of stack unchanged
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            mem_addr <= (incIncSp);
            mem_readEnable <= 1'b 1;
            sp <= incSp;
            state <= State_Popped;
          end
        end
        Insn_Or : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            stackA <= stackA | stackB;
            mem_readEnable <= 1'b 1;
            mem_addr <= (incIncSp);
            sp <= incSp;
            state <= State_Popped;
          end
        end
        Insn_And : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            stackA <= stackA & stackB;
            mem_readEnable <= 1'b 1;
            mem_addr <= (incIncSp);
            sp <= incSp;
            state <= State_Popped;
          end
        end
        Insn_Eq : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            binaryOpResult <= {(((wordSize - 1))-((0))+1){1'b0}};
            if((stackA == stackB)) begin
              binaryOpResult[0] <= 1'b 1;
            end
            state <= State_BinaryOpResult;
          end
        end
        Insn_Ulessthan : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            binaryOpResult <= {(((wordSize - 1))-((0))+1){1'b0}};
            if((stackA < stackB)) begin
              binaryOpResult[0] <= 1'b 1;
            end
            state <= State_BinaryOpResult;
          end
        end
        Insn_Ulessthanorequal : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            binaryOpResult <= {(((wordSize - 1))-((0))+1){1'b0}};
            if((stackA <= stackB)) begin
              binaryOpResult[0] <= 1'b 1;
            end
            state <= State_BinaryOpResult;
          end
        end
        Insn_Lessthan : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            binaryOpResult <= {(((wordSize - 1))-((0))+1){1'b0}};
            if ($signed(stackA) < $signed(stackB)) begin
              binaryOpResult[0] <= 1'b 1;
            end
            state <= State_BinaryOpResult;
          end
        end
        Insn_Lessthanorequal : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            binaryOpResult <= {(((wordSize - 1))-((0))+1){1'b0}};
            if ($signed(stackA) <= $signed(stackB)) begin
              binaryOpResult[0] <= 1'b 1;
            end
            state <= State_BinaryOpResult;
          end
        end
        Insn_Load : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            state <= State_Load2;
            mem_addr <= stackA[maxAddrBitIncIO:minAddrBit];
            mem_readEnable <= 1'b 1;
            //if (stackA[maxAddrBitIncIO:minAddrBit]<<2 == 'h8004)
            //    $display("### %08x load 8004", pc);
          end
        end
        Insn_Dup : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            pc <= pc + 1;
            sp <= decSp;
            stackB <= stackA;
            mem_write <= (stackB);
            mem_addr <= (incSp);
            mem_writeEnable <= 1'b 1;
          end
        end
        Insn_DupStackB : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            pc <= pc + 1;
            sp <= decSp;
            stackA <= stackB;
            stackB <= stackA;
            mem_write <= (stackB);
            mem_addr <= (incSp);
            mem_writeEnable <= 1'b 1;
          end
        end
        Insn_Store : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            pc <= pc + 1;
            mem_addr <= (stackA[maxAddrBitIncIO:minAddrBit]);
            mem_write <= (stackB);
            mem_writeEnable <= 1'b 1;
            sp <= incIncSp;
            state <= State_Resync;
          end
        end
        Insn_PopSP : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            pc <= pc + 1;
            mem_write <= (stackB);
            mem_addr <= (incSp);
            mem_writeEnable <= 1'b 1;
            sp <= stackA[maxAddrBitIncIO:minAddrBit];
            state <= State_Resync;
          end
        end
        Insn_Nop : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          pc <= pc + 1;
          //$display("@ Insn_Nop pc was %x <= %x", pc, pc + 1);
        end
        Insn_Not : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          pc <= pc + 1;
          stackA <=  ~stackA;
        end
        Insn_Flip : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          pc <= pc + 1;
          for (integer i=0; i <= wordSize - 1; i = i + 1) begin
            stackA[i] <= stackA[wordSize - 1 - i];
          end
        end
        Insn_AddTop : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          pc <= pc + 1;
          stackA <= stackA + stackB;
        end
        Insn_Shift : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          pc <= pc + 1;
          stackA[wordSize - 1:1] <= stackA[wordSize - 2:0];
          stackA[0] <= 1'b 0;
        end
        Insn_PushSPadd : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          pc <= pc + 1;
          stackA <= {(((wordSize - 1))-((0))+1){1'b0}};
          stackA[maxAddrBitIncIO:minAddrBit] <= stackA[maxAddrBitIncIO - minAddrBit:0] + sp;
        end
        Insn_Neqbranch : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          // branches are almost always taken as they form loops
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          sp <= incIncSp;
          if((stackB != 0)) begin
            pc <= stackA[maxAddrBitIncIO:0] + pc;
          end
          else begin
            pc <= pc + 1;
          end
          // need to fetch stack again.                           
          state <= State_Resync;
        end
        Insn_Mult : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          begin_inst <= 1'b 1;
          idim_flag <= 1'b 0;
          multA <= stackA;
          multB <= stackB;
          state <= State_Mult2;
        end
        Insn_Break : begin
          $display("Break instruction encountered at pc=%x", pc);
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          o_break <= 1'b 1;
        end
        Insn_Loadb : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            state <= State_Loadb2;
            mem_addr <= (stackA[maxAddrBitIncIO:minAddrBit]);
            mem_readEnable <= 1'b 1;
          end
        end
        Insn_Storeb : begin
          if(in_mem_busy == 1'b 0) begin
            insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
            begin_inst <= 1'b 1;
            idim_flag <= 1'b 0;
            state <= State_Storeb2;
            mem_addr <= stackA[maxAddrBitIncIO:minAddrBit];
            mem_readEnable <= 1'b 1;
          end
        end
        default : begin
          insn <= decodedOpcode[nextPC[byteBits - 1:0]]; // svo
          sp <= {(((maxAddrBitIncIO))-((minAddrBit))+1){1'bX}};
          $display("Illegal instruction");
          o_break <= 1'b 1;
        end
        endcase
        // insn/State_Execute
      end
      State_StoreSP2 : begin
        if(in_mem_busy == 1'b 0) begin
          mem_addr <= (incSp);
          mem_readEnable <= 1'b 1;
          state <= State_Popped;
        end
      end
      State_LoadSP2 : begin
        if(in_mem_busy == 1'b 0) begin
          state <= State_LoadSP3;
          mem_readEnable <= 1'b 1;
          mem_addr <= sp + {25'b0,spOffset} + 1;
        end
      end
      State_LoadSP3 : begin
        if(in_mem_busy == 1'b 0) begin
          pc <= pc + 1;
          state <= State_Execute;
          stackB <= stackA;
          stackA <= mem_read;
        end
      end
      State_AddSP2 : begin
        if(in_mem_busy == 1'b 0) begin
          pc <= pc + 1;
          state <= State_Execute;
          stackA <= stackA + mem_read;
        end
      end
      State_Load2 : begin
        mem_addr <= stackA[maxAddrBitIncIO:minAddrBit];
        if(in_mem_busy == 1'b 0) begin
          stackA <= mem_read;
          pc <= pc + 1;
          state <= State_Execute;
          //if (stackA[maxAddrBitIncIO:minAddrBit]<<2 == 'h8004)
          //    $display("### %08x load 8004 mem_read=%08x", pc, mem_read);
        end
      end
      State_Loadb2 : begin
        mem_addr <= stackA[maxAddrBitIncIO:minAddrBit];
        if(in_mem_busy == 1'b 0) begin
          stackA <= 0;
          stackA[7:0] <= mem_byte[stackA[byteBits-1:0]];
          pc <= pc + 1;
          state <= State_Execute;
        end
      end
      State_Storeb2 : begin
        if(in_mem_busy == 1'b 0) begin
          mem_addr <= stackA[maxAddrBitIncIO:minAddrBit];
          mem_write <= 
              (mem_read & ~(32'hff << (byteofs * 8))) |
              ({24'b0,stackB[7:0]} << (byteofs * 8));
          mem_writeEnable <= 1'b 1;
          pc <= pc + 1;
          sp <= incIncSp;
          state <= State_Resync;
        end
      end
      State_Fetch : begin
        if(in_mem_busy == 1'b 0) begin
          mem_addr <= pc[maxAddrBitIncIO:minAddrBit];
          mem_readEnable <= 1'b 1;
          state <= State_Decode;
        end
      end
      State_Mult2 : begin
        state <= State_Mult3;
      end
      State_Mult3 : begin
        state <= State_Mult4;
      end
      State_Mult4 : begin
        state <= State_Mult5;
      end
      State_Mult5 : begin
        if(in_mem_busy == 1'b 0) begin
          stackA <= multResult3;
          mem_readEnable <= 1'b 1;
          mem_addr <= (incIncSp);
          sp <= incSp;
          state <= State_Popped;
        end
      end
      State_BinaryOpResult : begin
        state <= State_BinaryOpResult2;
      end
      State_BinaryOpResult2 : begin
        mem_readEnable <= 1'b 1;
        mem_addr <= (incIncSp);
        sp <= incSp;
        stackA <= binaryOpResult2;
        state <= State_Popped;
      end
      State_Popped : begin
        if(in_mem_busy == 1'b 0) begin
          pc <= pc + 1;
          stackB <= (mem_read);
          state <= State_Execute;
        end
      end
      default : begin
        sp <= {(((maxAddrBitIncIO))-((minAddrBit))+1){1'bX}};
        //svo report "Illegal state" severity failure;
        o_break <= 1'b 1;
      end
      endcase
      // state
    end
  end
endmodule

// decode 4 instructions in parallel
module decoder
#(wordSize=32, wordBytes=4, OpCode_Size=8)
(
    input wire [wordSize-1:0] decodeWord,
    output reg [7:0] opcode[0:wordBytes - 1],
    output insn_type tDecodedOpcode[0:wordBytes-1]
);

   reg [OpCode_Size - 1:0] tOpcode;
   reg [4:0] tSpOffset;
   insn_type tNextInsn;

   for (genvar i = 0; i < wordBytes; i = i + 1) 
   always_comb begin
       tOpcode = decodeWord[(wordBytes - 1 - i + 1) * 8 - 1:(wordBytes - 1 - i) * 8];
       tSpOffset[4] =  ~tOpcode[4];
       tSpOffset[3:0] = (tOpcode[3:0]);
       opcode[i] = tOpcode;
       if((tOpcode[7:7] == OpCode_Im)) begin
           tNextInsn = Insn_Im;
       end
       else if((tOpcode[7:5] == OpCode_StoreSP)) begin
           if(tSpOffset == 0) begin
               tNextInsn = Insn_Pop;
           end
           else if(tSpOffset == 1) begin
               tNextInsn = Insn_PopDown;
           end
           else begin
               tNextInsn = Insn_StoreSP;
           end
       end
       else if (tOpcode[7:5] == OpCode_LoadSP) begin
           if(tSpOffset == 0) begin
               tNextInsn = Insn_Dup;
           end
           else if(tSpOffset == 1) begin
               tNextInsn = Insn_DupStackB;
           end
           else begin
               tNextInsn = Insn_LoadSP;
           end
       end
       else if((tOpcode[7:5] == OpCode_Emulate)) begin
           tNextInsn = Insn_Emulate;
           if(tOpcode[5:0] == OpCode_Neqbranch) begin
               tNextInsn = Insn_Neqbranch;
           end
           else if(tOpcode[5:0] == OpCode_Eq) begin
               tNextInsn = Insn_Eq;
           end
           else if(tOpcode[5:0] == OpCode_Lessthan) begin
               tNextInsn = Insn_Lessthan;
           end
           else if(tOpcode[5:0] == OpCode_Lessthanorequal) begin
               //tNextInsn :=Insn_Lessthanorequal;
           end
           else if(tOpcode[5:0] == OpCode_Ulessthan) begin
               tNextInsn = Insn_Ulessthan;
           end
           else if(tOpcode[5:0] == OpCode_Ulessthanorequal) begin
               //tNextInsn :=Insn_Ulessthanorequal;
           end
           else if(tOpcode[5:0] == OpCode_Loadb) begin
               tNextInsn = Insn_Loadb;
           end
           else if(tOpcode[5:0] == OpCode_Mult) begin
               tNextInsn = Insn_Mult;
           end
           else if(tOpcode[5:0] == OpCode_Storeb) begin
               tNextInsn = Insn_Storeb;
           end
           else if(tOpcode[5:0] == OpCode_Pushspadd) begin
               tNextInsn = Insn_PushSPadd;
           end
           else if(tOpcode[5:0] == OpCode_Callpcrel) begin
               tNextInsn = Insn_CallPCrel;
           end
           else if(tOpcode[5:0] == OpCode_Call) begin
               //tNextInsn :=Insn_Call;
           end
           else if(tOpcode[5:0] == OpCode_Sub) begin
               tNextInsn = Insn_Sub;
           end
           else if(tOpcode[5:0] == OpCode_PopPCRel) begin
               //tNextInsn :=Insn_PopPCrel;
           end
       end
       else if((tOpcode[7:4] == OpCode_AddSP)) begin
           if(tSpOffset == 0) begin
               tNextInsn = Insn_Shift;
           end
           else if(tSpOffset == 1) begin
               tNextInsn = Insn_AddTop;
           end
           else begin
               tNextInsn = Insn_AddSP;
           end
       end
       else begin
           case(tOpcode[3:0])
               OpCode_Nop : begin
                   tNextInsn = Insn_Nop;
               end
               OpCode_PushSP : begin
                   tNextInsn = Insn_PushSP;
               end
               OpCode_PopPC : begin
                   tNextInsn = Insn_PopPC;
               end
               OpCode_Add : begin
                   tNextInsn = Insn_Add;
               end
               OpCode_Or : begin
                   tNextInsn = Insn_Or;
               end
               OpCode_And : begin
                   tNextInsn = Insn_And;
               end
               OpCode_Load : begin
                   tNextInsn = Insn_Load;
               end
               OpCode_Not : begin
                   tNextInsn = Insn_Not;
               end
               OpCode_Flip : begin
                   tNextInsn = Insn_Flip;
               end
               OpCode_Store : begin
                   tNextInsn = Insn_Store;
               end
               OpCode_PopSP : begin
                   tNextInsn = Insn_PopSP;
               end
               default : begin
                   tNextInsn = Insn_Break;
               end
           endcase
       end
       tDecodedOpcode[i] = tNextInsn;
   end
endmodule
