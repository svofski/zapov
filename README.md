A port of ZPU4 to SystemVerilog
===============================

This version of ZPU is based on zpu_core.vhd.

Used vhd2vl to translate parts of the original VHDL. Handcrafted into 
working SystemVerilog code.

### Run test in Verilator

Edit env.sh so that ZPU toolchain is in the path. 

```
# set up paths to zpu toolchain
source verilator/sw/env.sh
# build and run a simple test
make run
# build and run uIP webserver demo
# needs tun/tap accessible to current user:
#   tunctl -u $USER && ifconfig tap0 192.168.1.1
# zpu ip is 192.168.1.2
make run-uip
```
